export const state = () => ({
  searchQuery: "",
});

export const getters = {
  searchFilter: (state, getters, rootState, rootGetters) => {
    return rootGetters["notes/orderedNotes"].filter((item) => {
      if (
        (item.title && item.title.includes(state.searchQuery)) ||
        (item.text && item.text.includes(state.searchQuery))
      ) {
        return item;
      }
    });
  },
};
export const mutations = {
  setSearchQuery(state, text) {
    state.searchQuery = text;
  },
};
