export const state = () => ({
  showPostComposer: false,
});

export const mutations = {
  setShowPostComposer(state, v) {
    state.showPostComposer = v;
  },
};
