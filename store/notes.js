import { uuid4 } from "../utils/uuid";

export const state = () => ({
  notes: [],
});

export const getters = {
  pinnedNotes: (state) => {
    return state.notes.filter((n) => n.pinned);
  },
  orderedPinnedNotes: (state, getters) => {
    return getters.pinnedNotes.reverse();
  },
  unpinnedNotes: (state) => {
    return state.notes.filter((n) => !n.pinned);
  },
  orderedUnpinnedNotes: (state) => {
    return state.notes.filter((n) => !n.pinned).reverse();
  },
  orderedNotes: (state) => {
    return state.notes.slice().reverse();
  },
};

export const actions = {
  addNote({ state, commit }) {},
  save({ state }) {
    if (window.localStorage) {
      window.localStorage.setItem(
        "_vuekeep.notes",
        JSON.stringify(state.notes)
      );
    } else {
      console.error(`unable to use localStorage: ${window.localStorage}`);
    }
  },
  load({ state, commit }) {
    if (window.localStorage) {
      commit(
        "setNotes",
        JSON.parse(window.localStorage.getItem("_vuekeep.notes")) || []
      );
    } else {
      console.error(`unable to use localStorage: ${window.localStorage}`);
    }
  },
};

export const mutations = {
  create(state, note) {
    state.notes.push({ ...note, id: uuid4(), pinned: false });
  },
  add(state, note) {
    state.notes.push(note);
  },
  update(state, { id, title, text, pinned }) {
    const index = state.notes.findIndex((n) => n.id === id);
    state.notes[index].title = title;
    state.notes[index].text = text;
    state.notes[index].pinned = pinned;
  },
  setNotes(state, notes = []) {
    state.notes = notes;
  },
  remove(state, id) {
    const index = state.notes.findIndex((n) => n.id === id);
    state.notes.splice(index, 1);
  },
};
